package com.example.ishbor.login_register;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ishbor.R;
import com.example.ishbor.model.User;
import com.example.ishbor.network.ApiClient;
import com.example.ishbor.network.ApiService;
import com.example.ishbor.utils.Token;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;
public class RegisterActivity extends AppCompatActivity implements check{
    public static final String TAG="MainActivity";
    private static Token token;
    private ApiService apiService;
    private RelativeLayout relativeLayout;
    private Context context;
    private ImageView image;
    private TextView logoText, sloganText;
    private CompositeDisposable disposable = new CompositeDisposable();
    TextInputEditText textFullname,textUsername,textPassword,textPhoneNumber;
    String surname,username,pass,number;
    private Button register,login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_register);
        token=new Token(this);
        login=findViewById(R.id.already_registered);
        relativeLayout=findViewById(R.id.relativelayout_register);
        image = findViewById(R.id.image_name);
        logoText = findViewById(R.id.welcome_register);
        sloganText = findViewById(R.id.slogan_register);
        textFullname=findViewById(R.id.surname);
        textUsername=findViewById(R.id.username);
        textPassword=findViewById(R.id.password);
        textPhoneNumber=findViewById(R.id.phone);
        register=findViewById(R.id.register_save);
        apiService = ApiClient.getClient().create(ApiService.class);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registration();
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RegisterActivity.this,LoginActivity.class);
                Pair[] pairs = new Pair[7];
                pairs[0] = new Pair<View, String>(image, "logo_image");
                pairs[1] = new Pair<View, String>(logoText, "logo_text");
                pairs[2] = new Pair<View, String>(sloganText, "logo_desc");
                pairs[3] = new Pair<View, String>(textPhoneNumber, "username_tran");
                pairs[4] = new Pair<View, String>(textPassword, "password_tran");
                pairs[5] = new Pair<View, String>(register, "button_tran");
                pairs[6] = new Pair<View, String>(login, "login_signup_tran");
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                   ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(RegisterActivity.this, pairs);
                   startActivity(intent, options.toBundle());
                }
            }
        });
    }

    private void registration(){
        surname=textFullname.getText().toString().trim();
        username=textUsername.getText().toString().trim();
        pass=textPassword.getText().toString().trim();
        number=textPhoneNumber.getText().toString().trim();
        Log.d(TAG,surname+username+number+pass);
        disposable.add(
             apiService.register(surname,username,number,pass)
             .subscribeOn(Schedulers.io())
             .observeOn(AndroidSchedulers.mainThread())
             .subscribeWith(new DisposableSingleObserver<User>(){
                 @Override
                 public void onSuccess(User user) {
                     Log.d(TAG,"success"+user.getMessage());
                     switch (user.getMessage()){
                         case "inserted":
                             Log.d(TAG,user.getToken());
                             token.setDisplayToken(user.getToken());
                             token.setDisplayId(user.getId());
                             token.setLoginStatus(true);
                             Log.d(TAG,"Id "+Integer.toString(user.getId()));
                             finish();
                              StyleableToast.makeText(getApplicationContext(),"Registratsiyadan o'tildi",R.style.exampleToast).show();
                             break;
                         case "exists":
                             StyleableToast.makeText(getApplicationContext(),"Bu Profil Mavjud !!!",R.style.exist).show();
                             break;
                         case "mistake":
                             StyleableToast.makeText(getApplicationContext(),"Malumotda xatolik bor !!!",R.style.errorToast).show();
                             break;
                          default:
                              StyleableToast.makeText(getApplicationContext(),"Unknown error !!!",R.style.errorToast).show();
                              break;
                     }

                 }
                 @Override
                 public void onError(Throwable e) {
                     Log.e(TAG, "onError: " + e.getMessage());
                     Log.d(TAG,surname+username+number+pass);
                     showErrors(e,relativeLayout,getApplicationContext());
                 }

             }));
    }

    @Override
    public void showErrors(Throwable e, RelativeLayout relativeLayout, Context context) {
        String message = "";
        try {
            if (e instanceof IOException) {
                message = "Internet sozligini tekshiring!";
            } else if (e instanceof HttpException) {
                HttpException error = (HttpException) e;
                String errorBody = error.response().errorBody().string();
                JSONObject jObj = new JSONObject(errorBody);

                message = jObj.getString("error");
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (JSONException e1) {
            e1.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        if (TextUtils.isEmpty(message)) {
            message = "Unknown error occurred! Xatolik sodir bo'ldi.";
        }

        Snackbar snackbar = Snackbar
                .make(relativeLayout, message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    @Override
    public void logout() {
        token.setLoginStatus(false);
        token.setDisplayToken("null");
        token.setDisplayId(0);
    }
}
