package com.example.ishbor.login_register;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;

import com.example.ishbor.R;

public class RetailerStartupScreen extends AppCompatActivity implements View.OnClickListener{

    private Button btn_login,btn_register,btn_how;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_retailer_startup_screen);
        btn_login=findViewById(R.id.btn_login);
        btn_register=findViewById(R.id.btn_register);
        btn_register.setOnClickListener(this);
        btn_login.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {
      switch (v.getId()){
          case R.id.btn_login:
          startActivity(new Intent(this,LoginActivity.class));
          break;

          case R.id.btn_register:
              startActivity(new Intent(this,RegisterActivity.class));
              break;
      }
    }
}