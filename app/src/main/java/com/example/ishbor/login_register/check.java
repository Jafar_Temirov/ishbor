package com.example.ishbor.login_register;

import android.content.Context;
import android.widget.RelativeLayout;

public interface check {
     void showErrors(Throwable e, RelativeLayout relativeLayout, Context context);
     void  logout();
}
