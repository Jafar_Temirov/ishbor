package com.example.ishbor.login_register;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ishbor.R;
import com.example.ishbor.model.User;
import com.example.ishbor.network.ApiClient;
import com.example.ishbor.network.ApiService;
import com.example.ishbor.utils.Functions;
import com.example.ishbor.utils.Token;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class LoginActivity extends AppCompatActivity {
    public static final String TAG="MainActivity";
    private CompositeDisposable disposable = new CompositeDisposable();
    private ApiService apiService;
    private RelativeLayout relativeLayout;
    private Token token;
    Button callSignUp, login_btn;
    ImageView image;
    TextView logoText, sloganText;
    TextInputEditText phone, password;
    String phoneString,pass;
    RegisterActivity registerActivity=new RegisterActivity();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        token=new Token(this);
        relativeLayout=findViewById(R.id.relativelayout_login);
        callSignUp = findViewById(R.id.regist_btn);
        image = findViewById(R.id.logo_image);
        logoText = findViewById(R.id.logo_name);
        sloganText = findViewById(R.id.slogan_name);
        phone = findViewById(R.id.log_phonenumber);
        password = findViewById(R.id.log_password);
        login_btn = findViewById(R.id.log_btn);
        apiService = ApiClient.getClient().create(ApiService.class);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
        callSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                Pair[] pairs = new Pair[7];
                pairs[0] = new Pair<View, String>(image, "logo_image");
                pairs[1] = new Pair<View, String>(logoText, "logo_text");
                pairs[2] = new Pair<View, String>(sloganText, "logo_desc");
                pairs[3] = new Pair<View, String>(phone, "username_tran");
                pairs[4] = new Pair<View, String>(password, "password_tran");
                pairs[5] = new Pair<View, String>(login_btn, "button_tran");
                pairs[6] = new Pair<View, String>(callSignUp, "login_signup_tran");
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(LoginActivity.this, pairs);
                    startActivity(intent, options.toBundle());
                }
            }
        });
    }
    private void login(){
        phoneString=phone.getText().toString().trim();
        pass=password.getText().toString().trim();
        Log.d(TAG,pass+phoneString);
        disposable.add(
                apiService.login(phoneString,pass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<User>(){
                    @Override
                    public void onSuccess(User user) {
                        Log.d(TAG,"success"+user.getMessage());
                        switch (user.getMessage()){
                            case "exist":
                                Log.d(TAG,user.getToken());
                                token.setDisplayToken(user.getToken());
                                token.setDisplayId(user.getId());
                                token.setLoginStatus(true);
                                finish();
                                StyleableToast.makeText(getApplicationContext(),"Login Qabul qilindi",R.style.exampleToast).show();
                                break;
                            case "error":
                                StyleableToast.makeText(getApplicationContext(),"Bu Profil Allaqachon\n ochilgan !!!",R.style.exist).show();
                                break;
                            default:
                                StyleableToast.makeText(getApplicationContext(),"Unknown error !!!",R.style.errorToast).show();
                                break;
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG,"error "+e.getLocalizedMessage());
                         registerActivity.showErrors(e,relativeLayout,getApplicationContext());
                    }
                }));
    }
}
