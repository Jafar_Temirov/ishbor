package com.example.ishbor.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.ishbor.R;

public class Token {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context context;

    public Token(Context context) {
        this.context=context;
        sharedPreferences=context.getSharedPreferences(String.valueOf(R.string.s_pref_file), Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
    }
    //Setting login status
    public void setLoginStatus(boolean status){
        editor.putBoolean(String.valueOf(R.string.s_pref_login_status), status);
        editor.commit();
    }

    public boolean getLoginStatus(){
        return sharedPreferences.getBoolean(String.valueOf(R.string.s_pref_login_status), false);
    }

    //For Token
    public void setDisplayToken(String token){
        editor.putString(("API_KEY"),token);
        editor.commit();
    }
    public String getDisplayToken(){
        return sharedPreferences.getString("API_KEY","null");
    }

    // For Id
    public void setDisplayId(int user_id){
        editor.putInt(("user_id"),user_id);
        editor.commit();
    }

    public int getDisplayId(){
        return sharedPreferences.getInt("user_id",0);
    }



}
