package com.example.ishbor.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.ishbor.R;
import com.google.android.material.snackbar.Snackbar;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

public class Functions {

    public void snackbar(RelativeLayout relativeLayout, String snack_text){
        Snackbar snackbar = Snackbar
                .make(relativeLayout, snack_text, Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(Color.MAGENTA)
                .setDuration(8000);
        snackbar.show();
    }

    public void showAlertDialog(final Context context, String title, String message, String positive, String negative){
        if (context!=null){
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(title)
                    .setMessage(message);

            builder.setPositiveButton(positive, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    StyleableToast.makeText(context,"Qabul qilindi", R.style.exampleToast).show();

                }
            });
            builder.setNegativeButton(negative, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            builder.show();
        }
    }

}
