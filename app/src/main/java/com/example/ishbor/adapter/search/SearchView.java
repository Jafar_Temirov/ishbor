package com.example.ishbor.adapter.search;

import com.example.ishbor.model.Job_information;

import java.util.List;

public interface SearchView {
    void showLoading();
    void hideLoading();
    void onGetResult(List<Job_information> notes);
}
