package com.example.ishbor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ishbor.R;
import com.example.ishbor.model.Job_information;
import java.util.List;

public class ElonAdapter extends RecyclerView.Adapter<ElonAdapter.ElonViewHolder> {
    private List<Job_information> job_informations;
    private Context context;
    private ItemClickListener itemClickListener;

    public ElonAdapter(List<Job_information> job_informations, Context context,ItemClickListener itemClickListener) {
        this.job_informations = job_informations;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ElonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.row_item_elon,parent,false);
        return new ElonViewHolder(view,itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ElonViewHolder holder, int position) {
      Job_information job_information=job_informations.get(position);
        holder.name.setText(job_information.getJob_title());
        holder.taxrirlash.setText(job_information.getTaxrirlash());
        holder.date.setText(job_information.getInserted_date());
    }

    @Override
    public int getItemCount() {
        return job_informations.size();
    }

    public class ElonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView name,taxrirlash,date;
        ItemClickListener itemClickListener;
        public ElonViewHolder(@NonNull View itemView,ItemClickListener itemClickListener) {
            super(itemView);
            name=itemView.findViewById(R.id.elon_nomi);
            taxrirlash=itemView.findViewById(R.id.elon_taxrirlash);
            date=itemView.findViewById(R.id.elon_vaqt);
            this.itemClickListener=itemClickListener;
        }
        @Override
        public void onClick(View v) {
            itemClickListener.onItemClick(v, getAdapterPosition());
        }

    }

    public interface ItemClickListener{
        void onItemClick(View view, int position);
    }
}
