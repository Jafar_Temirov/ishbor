package com.example.ishbor.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private final List<Fragment> fragmentList= new ArrayList<>();
    private final List<String> stringList=new ArrayList<>();

    public ViewPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return stringList.size();
    }
    @Override
    public  CharSequence getPageTitle(int position)
    {
     return    stringList.get(position);
    }

    public void AddFragment(Fragment fragment, String string){
        fragmentList.add(fragment);
        stringList.add(string);
    }
}
