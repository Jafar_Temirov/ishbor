package com.example.ishbor.adapter;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.ishbor.R;
import com.example.ishbor.model.Job_information;

import java.util.List;

public class    MainAdapter  extends RecyclerView.Adapter<MainAdapter.MainViewHolder> {
    private List<Job_information>  jobInformations;
    private Context context;
    private ItemClickListener itemClickListener;

    public MainAdapter( Context context, List<Job_information> jobInformations,ItemClickListener itemClickListener) {
        this.jobInformations = jobInformations;
        this.context = context;
        this.itemClickListener=itemClickListener;
    }
    @NonNull
    @Override
    public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.row_item_main,parent,false);
        return new MainViewHolder(view,itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MainViewHolder holder, int position) {
        Job_information job_information=jobInformations.get(position);
        holder.cardView.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        holder.textView1.setText(job_information.getJob_title());
        holder.textView3.setText("Ish haqqi:  "+job_information.getCosts());
        holder.textView5.setText("Ta\'rif:  "+job_information.getJob_description());

    }

    @Override
    public int getItemCount() {return jobInformations.size();}

    public class MainViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView textView1,textView3,textView5;
        ItemClickListener itemClickListener;
        CardView cardView;


        public MainViewHolder(@NonNull View itemView,ItemClickListener itemClickListener2) {
            super(itemView);
            itemView.setOnClickListener(this);
            cardView=itemView.findViewById(R.id.card_item_main);
            textView1=itemView.findViewById(R.id.job_title);
            textView3=itemView.findViewById(R.id.job_costs);
            textView5=itemView.findViewById(R.id.ish_tarif);
            itemClickListener=itemClickListener2;
            cardView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v)
        { switch (v.getId()) {
            case R.id.card_item_main:
                itemClickListener.onItemClick(v, getAdapterPosition());
                break;
            default:
                break;
         }
        }
    }

    public interface ItemClickListener{
        void onItemClick(View view,int position);
    }
}
