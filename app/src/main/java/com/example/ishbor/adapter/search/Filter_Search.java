package com.example.ishbor.adapter.search;

import android.widget.Filter;
import com.example.ishbor.model.Job_information;
import java.util.ArrayList;
import java.util.List;

public class Filter_Search extends Filter {
    SearchAdapter searchAdapter;
    ArrayList<Job_information> jobInformations;

    public Filter_Search(SearchAdapter searchAdapter, ArrayList<Job_information> jobInformations) {
        this.searchAdapter = searchAdapter;
        this.jobInformations = jobInformations;
    }

    @Override
    protected Filter.FilterResults performFiltering(CharSequence constraint) {
        Filter.FilterResults results=new Filter.FilterResults();
        if (constraint!=null&&constraint.length()>0){
            constraint=constraint.toString().toUpperCase();
            ArrayList<Job_information> job_informations=new ArrayList<>();

            for (int i=0;i<jobInformations.size();i++)
            {
                //CHECK
                if(jobInformations.get(i).getJob_title().toUpperCase().contains(constraint)) {
                    job_informations.add(jobInformations.get(i));
                }
                if (jobInformations.get(i).getJob_description().toUpperCase().contains(constraint)){
                    job_informations.add(jobInformations.get(i));
                }
            }

            results.count=jobInformations.size();
            results.values=jobInformations;

        }
        else
        {
            results.count=jobInformations.size();
            results.values=jobInformations;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
        searchAdapter.informations=(List<Job_information>)results.values;
        searchAdapter.notifyDataSetChanged();
    }
}
