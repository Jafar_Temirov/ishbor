package com.example.ishbor.adapter.search;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ishbor.R;
import com.example.ishbor.model.Job_information;

import java.util.ArrayList;
import java.util.List;

import static com.example.ishbor.login_register.LoginActivity.TAG;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> implements Filterable {
   private Context context;
   public List<Job_information> informations;
   List<Job_information> searchInformation;
   private ItemClickListener itemClickListener;
   Filter_Search filter_search;

    public SearchAdapter(Context context, List<Job_information> informations, ItemClickListener itemClickListener) {
        this.context = context;
        this.informations = informations;
        this.itemClickListener = itemClickListener;
        this.searchInformation=informations;
    }

    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.row_item_main,parent,false);
        return new SearchViewHolder(view,itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        Job_information job_information=searchInformation.get(position);
        holder.cardView.setAnimation(AnimationUtils.loadAnimation(context,R.anim.fade_scale_animation));
        holder.textView1.setText(job_information.getJob_title());
        holder.textView3.setText("Ish haqqi:  "+job_information.getCosts());
        holder.textView5.setText("Ta\'rif:  "+job_information.getJob_description());
    }

    @Override
    public int getItemCount() {
        return searchInformation.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String key=constraint.toString();
                if (key.isEmpty()){
                    searchInformation=informations;
                }else {
                    List<Job_information> list=new ArrayList<>();
                    for (Job_information information:informations){
                        if (information.getJob_title().toLowerCase().contains(key.toLowerCase())||information.getJob_description().toLowerCase().contains(key.toLowerCase())){
                            list.add(information);
                        }
                    }

                    searchInformation=list;
                }
                FilterResults results=new FilterResults();
                results.values=searchInformation;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
              searchInformation= (List<Job_information>) results.values;
              notifyDataSetChanged();
            }
        };
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView textView1,textView3,textView5;
        ItemClickListener itemClickListener;
        CardView cardView;
        public SearchViewHolder(@NonNull View itemView,ItemClickListener itemClickListener) {
            super(itemView);
            cardView=itemView.findViewById(R.id.card_item_main);
            textView1=itemView.findViewById(R.id.job_title);
            textView3=itemView.findViewById(R.id.job_costs);
            textView5=itemView.findViewById(R.id.ish_tarif);
            this.itemClickListener=itemClickListener;
            cardView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemClickListener.onItemClick(v,getAdapterPosition());
        }
    }

    public interface ItemClickListener{
        void onItemClick(View view,int position);

    }

}
