package com.example.ishbor.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.ishbor.R;
import com.example.ishbor.model.Chat;
import com.example.ishbor.utils.Token;

import java.util.List;

import static com.example.ishbor.login_register.RegisterActivity.TAG;

public class TelegramAdapter  extends RecyclerView.Adapter<TelegramAdapter.TelegramViewHolder> {
    private List<Chat> chats;
    private Context context;
    private CardView cardView;
    private Token token;

    public TelegramAdapter(Context context,List<Chat> chats) {
        this.chats = chats;
        this.context = context;
    }

    @NonNull
    @Override
    public TelegramAdapter.TelegramViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.row_item_telegram,parent,false);
        return new TelegramViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TelegramAdapter.TelegramViewHolder holder, int position) {
          Chat chat=chats.get(position);
        int data;
          data=token.getDisplayId();
          Log.d(TAG,"Adapter is xcalled"+Integer.toString(data));
          if (data==Integer.parseInt(chat.getUser1_id())){
              holder.text_user1.setText(chat.getText());
              holder.time1.setText(chat.getChat_date());
              cardView.setCardBackgroundColor(Color.parseColor("#ffcfcf"));
          }else{
              holder.text_user2.setText(chat.getText());
              holder.time2.setText(chat.getChat_date());
              cardView.setCardBackgroundColor(Color.parseColor("#7adccf"));
          }

    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    public class TelegramViewHolder extends RecyclerView.ViewHolder {
        TextView text_user1,text_user2,time1,time2;

        public TelegramViewHolder(@NonNull View itemView) {
            super(itemView);
            token=new Token(context);
            cardView=itemView.findViewById(R.id.card_telegram);
            text_user1=itemView.findViewById(R.id.text_user1);
            text_user2=itemView.findViewById(R.id.text_user2);
            time2=itemView.findViewById(R.id.text_user2_vaqt);
            time1=itemView.findViewById(R.id.text_user1_vaqt);
        }
    }
}
