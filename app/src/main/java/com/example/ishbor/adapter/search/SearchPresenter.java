package com.example.ishbor.adapter.search;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.ishbor.model.Job_information;
import com.example.ishbor.network.ApiClient;
import com.example.ishbor.network.ApiService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.ishbor.login_register.LoginActivity.TAG;

public class SearchPresenter {
    private SearchView searchView;

    public SearchPresenter(SearchView searchView) {
        this.searchView = searchView;
    }
   public void getData(){
        searchView.showLoading();
        ApiService apiInterface= ApiClient.getClient().create(ApiService.class);
        Call<List<Job_information>> call=apiInterface.getSearchData();
        call.enqueue(new Callback<List<Job_information>>() {
            @Override
            public void onResponse(@NonNull Call<List<Job_information>> call, @NonNull Response<List<Job_information>> response) {
                searchView.hideLoading();
                if (response.isSuccessful() && response.body()!=null){
                    searchView.onGetResult(response.body());
                }
            }
            @Override
            public void onFailure(@NonNull Call<List<Job_information>> call,@NonNull Throwable t) {
                searchView.hideLoading();
                Log.d(TAG,"Serverda xatolik");
            }
        });

    }
}
