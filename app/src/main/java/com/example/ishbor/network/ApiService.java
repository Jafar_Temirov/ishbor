package com.example.ishbor.network;

import com.example.ishbor.model.Category;
import com.example.ishbor.model.Chat;
import com.example.ishbor.model.Job_information;
import com.example.ishbor.model.User;
import com.example.ishbor.model.UyModel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {

    @FormUrlEncoded
    @POST("ishbor/register.php")
    Single<User> register(@Field("surname") String surname,
                          @Field("username") String username,
                          @Field("phone_number") String phone_number,
                          @Field("password") String password);
    @FormUrlEncoded
    @POST("ishbor/login.php")
    Single<User> login(@Field("phone_number") String phone_number,
                       @Field("password") String password);

    @GET("ishbor/main_getdata.php")
    Call<List<Job_information>> getAllData(@Query("job_type_id") int id);

    @GET("ishbor/favourite.php")
    Call<List<Job_information>> getFavourite();

    @GET("ishbor/search.php")
    Call<List<Job_information>> getSearchData();


    @GET("ishbor/UyMain.php")
    Call<List<UyModel>> getAllUyMain();

    @GET("ishbor/kategoriya.php")
    Call<List<Category>> getCategory();

    @GET("ishbor/chat.php")
    Call<List<Chat>> getChat(
            @Query("job_info_id") int job_info_id,
            @Query("token") String my_token,
            @Query("partner_id") int partner_id);

    @GET("ishbor/getElonlar.php")
    Call<List<Job_information>> getElonlar(@Query("token") String token);

    @FormUrlEncoded
    @POST("ishbor/saveChat.php")
    Call<Chat> saveChat(@Field("job_info_id") int job_info_id,
                          @Field("user1_id") String token,
                          @Field("id") int id,
                          @Field("user2_id") int user2_id,
                          @Field("text") String text);

    @FormUrlEncoded
    @POST("ishbor/updateUser.php")
    Call<User> updateUser(
                        @Field("surname")  String surname,
                        @Field("username") String username,
                        @Field("password") String password,
                        @Field("tel_number") String tel_number,
                        @Field("token") String token);


    @GET("ishbor/identifyUser.php")
    Call<User> IdentifyUser(@Query("token") String token);

    @FormUrlEncoded
    @POST("ishbor/saveJob.php")
    Call<Job_information> Save_Job(
            @Field("job_title") String job_title,
            @Field("job_description") String job_description,
            @Field("costs") String costs,
            @Field("tel_number") String tel_number,
            @Field("user_id") int user_id,
            @Field("longtitude") String longtitude,
            @Field("latitude") String latitude,
            @Field("job_started_date") String job_started_date,
            @Field("job_ended_date") String job_ended_date,
            @Field("job_category_id") int  job_category_id,
            @Field("job_type_id") int job_type_id);

    @FormUrlEncoded
    @POST("ishbor/update_love.php")
    Call<Job_information> updateLove(
            @Field("token") String token,
            @Field("job_info_id") int id,
            @Field("love") boolean love);
}
