package com.example.ishbor.Databases;

import android.os.AsyncTask;

public  class PopulateDbAsync extends AsyncTask<Void,Void,Void> {

    private final DaoMain mDao;

    PopulateDbAsync(MainDatabase db) {
        mDao = db.daoMain();
    }

    @Override
    protected Void doInBackground(final Void... params) {
        mDao.deleteAllVacancy();
        return null;
    }
}
