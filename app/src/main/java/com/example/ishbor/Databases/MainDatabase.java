package com.example.ishbor.Databases;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import com.example.ishbor.model.Job_information;

@Database(entities = {Job_information.class},version = 1,exportSchema = false)
public abstract class MainDatabase extends RoomDatabase {
    public abstract DaoMain daoMain();

    private static MainDatabase INSTANCE;

   public static MainDatabase getMainInformation(final Context context) {
            if (INSTANCE == null) {
                synchronized (MainDatabase.class) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        MainDatabase.class, "ishbor_db")
                        .addCallback(sRoomDatabaseCallback)
                        .build();
            }
        }
        return INSTANCE;
    }

    private static Callback sRoomDatabaseCallback =
            new Callback(){
                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };
}
