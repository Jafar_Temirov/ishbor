package com.example.ishbor.Databases;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.ishbor.model.Job_information;

import java.util.List;

@Dao
public interface DaoMain {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Job_information jobInformation);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertData(List<Job_information> jobInformation);

    @Update
    void update(Job_information jobInformation);
    @Delete
    void delete(Job_information jobInformation);
    //Delete all movies from table
    @Query("DELETE FROM job_info")
    void deleteAllVacancy();

    @Query("SELECT * from job_info ORDER BY id DESC")
    LiveData<List<Job_information>> getAllPosts();

}
