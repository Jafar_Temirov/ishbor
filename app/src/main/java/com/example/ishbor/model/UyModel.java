package com.example.ishbor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UyModel {

    @SerializedName("home_owner")
    @Expose
    private String home_owner;

    @SerializedName("tel_number")
    @Expose
    private String tel_number;

    @SerializedName("uy_haqida")
    @Expose
    private String uy_haqida;

    @SerializedName("uy_manzili")
    @Expose
    private String uy_manzili;

    @SerializedName("narxi")
    @Expose
    private String narxi;

    @SerializedName("uytype_id")
    @Expose
    private String uytype_id;

    @SerializedName("posted_at")
    @Expose
    private String posted_at;

    @SerializedName("image_row")
    @Expose
    private String image_row;

    public String getHome_owner() {
        return home_owner;
    }

    public void setHome_owner(String home_owner) {
        this.home_owner = home_owner;
    }

    public String getTel_number() {
        return tel_number;
    }

    public void setTel_number(String tel_number) {
        this.tel_number = tel_number;
    }

    public String getUy_haqida() {
        return uy_haqida;
    }

    public void setUy_haqida(String uy_haqida) {
        this.uy_haqida = uy_haqida;
    }

    public String getUy_manzili() {
        return uy_manzili;
    }

    public void setUy_manzili(String uy_manzili) {
        this.uy_manzili = uy_manzili;
    }

    public String getNarxi() {
        return narxi;
    }

    public void setNarxi(String narxi) {
        this.narxi = narxi;
    }

    public String getUytype_id() {
        return uytype_id;
    }

    public void setUytype_id(String uytype_id) {
        this.uytype_id = uytype_id;
    }

    public String getPosted_at() {
        return posted_at;
    }

    public void setPosted_at(String posted_at) {
        this.posted_at = posted_at;
    }

    public String getImage_row() {
        return image_row;
    }

    public void setImage_row(String image_row) {
        this.image_row = image_row;
    }
}
