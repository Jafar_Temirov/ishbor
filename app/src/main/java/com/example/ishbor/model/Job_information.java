package com.example.ishbor.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "job_info")
public class Job_information {
    @SerializedName("token")
    private String token;

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private int id;

    @NonNull
    @ColumnInfo(name = "job_title")
    @SerializedName("job_title")
    private String job_title;

    @NonNull
    @ColumnInfo(name = "job_description")
    @SerializedName("job_description")
    private String job_description;

    @NonNull
    @ColumnInfo(name = "costs")
    @SerializedName("costs")
    private String costs;

    @NonNull
    @ColumnInfo(name = "tel_number")
    @SerializedName("tel_number")
    private String tel_number;

    @SerializedName("love")
    private Boolean love;

    @SerializedName("longtitude")
    private String longtitude;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("job_started_date")
    private String job_started_date;

    @SerializedName("job_ended_date")
    private String job_ended_date;

    @SerializedName("user_id")
    private int user_id;

    @SerializedName("inserted_date")
    private String inserted_date;

    @SerializedName("taxrirlash")
    private String taxrirlash;

    @SerializedName("success")
    private Boolean success;

    @SerializedName("message")
    private String message;

    public String getTaxrirlash() {
        return taxrirlash;
    }

    public void setTaxrirlash(String taxrirlash) {
        this.taxrirlash = taxrirlash;
    }

    public String getInserted_date() {
        return inserted_date;
    }

    public void setInserted_date(String inserted_date) {
        this.inserted_date = inserted_date;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getJob_started_date() {
        return job_started_date;
    }

    public void setJob_started_date(String job_started_date) {
        this.job_started_date = job_started_date;
    }

    public String getJob_ended_date() {
        return job_ended_date;
    }

    public void setJob_ended_date(String job_ended_date) {
        this.job_ended_date = job_ended_date;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public Boolean getLove() {
        return love;
    }

    public void setLove(Boolean love) {
        this.love = love;
    }

    public Job_information(String job_title, String job_description, String costs, String tel_number) {
        this.job_title = job_title;
        this.job_description = job_description;
        this.costs = costs;
        this.tel_number = tel_number;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getJob_description() {
        return job_description;
    }

    public void setJob_description(String job_description) {
        this.job_description = job_description;
    }


    public String getCosts() {
        return costs;
    }

    public void setCosts(String costs) {
        this.costs = costs;
    }

    public String getTel_number() {
        return tel_number;
    }

    public void setTel_number(String tel_number) {
        this.tel_number = tel_number;
    }
}
