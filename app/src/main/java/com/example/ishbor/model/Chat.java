package com.example.ishbor.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Chat {
    @Expose
    @SerializedName("chat_id")
    private int chat_id;
    @Expose
    @SerializedName("job_info_id")
    private String job_info_id;
    @Expose
    @SerializedName("user1_id")
    private String user1_id;
    @Expose
    @SerializedName("user2_id")
    private String user2_id;
    @Expose
    @SerializedName("text")
    private String text;
    @Expose
    @SerializedName("chat_date")
    private String chat_date;

    @SerializedName("success")
    private Boolean success;

    @SerializedName("message")
    private String message;



    public String getChat_date() {
        return chat_date;
    }

    public void setChat_date(String chat_date) {
        this.chat_date = chat_date;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getChat_id() {
        return chat_id;
    }

    public void setChat_id(int chat_id) {
        this.chat_id = chat_id;
    }

    public String getJob_info_id() {
        return job_info_id;
    }

    public void setJob_info_id(String job_info_id) {
        this.job_info_id = job_info_id;
    }

    public String getUser1_id() {
        return user1_id;
    }

    public void setUser1_id(String user1_id) {
        this.user1_id = user1_id;
    }

    public String getUser2_id() {
        return user2_id;
    }

    public void setUser2_id(String user2_id) {
        this.user2_id = user2_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
