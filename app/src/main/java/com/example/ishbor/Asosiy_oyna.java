package com.example.ishbor;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.ishbor.fragment.fragment_bottom.FavouriteFragment;
import com.example.ishbor.fragment.SearchFragment;
import com.example.ishbor.fragment.fragment_drawer.AdminPanel;
import com.example.ishbor.fragment.fragment_bottom.Doimiy_ish;
import com.example.ishbor.fragment.fragment_drawer.Elon_fragment;
import com.example.ishbor.fragment.fragment_drawer.FullscreenDialog;
import com.example.ishbor.fragment.fragment_drawer.Haqida;
import com.example.ishbor.fragment.fragment_bottom.MainFragment;
import com.example.ishbor.fragment.fragment_drawer.Profile;
import com.example.ishbor.fragment.fragment_drawer.Xabarlar;
import com.example.ishbor.login_register.RetailerStartupScreen;
import com.example.ishbor.utils.BottomNavigationViewBehavior;
import com.example.ishbor.utils.Functions;
import com.example.ishbor.utils.Token;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;
public class Asosiy_oyna extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    // Drawer Menu
    private Token token;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    ImageView menu_item,reg_log_icon;
    static final float END_SCALE = 0.7f;
    RelativeLayout contentView;
    FrameLayout frameLayout;
    Functions functions=new Functions();
    private BottomNavigationView mBottomNavigationView;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_asosiy_oyna);
        contentView = findViewById(R.id.content);
        token=new Token(this);
        coordinatorLayout=findViewById(R.id.coordinator_layout);
        drawerLayout=findViewById(R.id.drawer_layout);
        mBottomNavigationView =findViewById(R.id.bottom_navigation);
        navigationView=findViewById(R.id.navigation_view);
        menu_item=findViewById(R.id.menu_icon);
        reg_log_icon=findViewById(R.id.reg_log_icon);
        reg_log_icon.setColorFilter(0xFFFF0000, PorterDuff.Mode.MULTIPLY);
        frameLayout=findViewById(R.id.fragment_container);
        if (frameLayout != null) {
            if (savedInstanceState != null) {
                return;
            }
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, new MainFragment())
                    .commit();
        }
        Menu  menu=navigationView.getMenu();
        if (token.getLoginStatus()){
             menu.findItem(R.id.register_login).setVisible(false);
             menu.findItem(R.id.logout).setVisible(true);
        }else {
            menu.findItem(R.id.register_login).setVisible(true);
            menu.findItem(R.id.logout).setVisible(false);
        }

        navigationDraver();
        BottomNavigation();
        // Permission for external storage
        checkPermissionForReadExtertalStorage();

        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mBottomNavigationView.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationViewBehavior());
     }
     private void BottomNavigation(){
        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.bottom_home:
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.fragment_container, new MainFragment())
                                .commit();
                        break;
                    case R.id.bottom_doimiyish:
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.fragment_container, new Doimiy_ish())
                                .commit();
                        break;
                    case R.id.bottom_favourite:
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.fragment_container, new FavouriteFragment())
                                .commit();
                        break;
                }

                return false;
            }
        });
    }

     private void navigationDraver() {
        navigationView.bringToFront();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_home);
        menu_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerVisible(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);

                else
                    drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        reg_log_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialog3 = new SearchFragment();
                dialog3.show(getSupportFragmentManager(), "jafar");
            }
        });

        animateNavigationDrawer();
    }

     public void callRetailScreens(View view){
     startActivity(new Intent(getApplicationContext(), RetailerStartupScreen.class));
    }

     @Override
     public void onBackPressed() {
        if (drawerLayout.isDrawerVisible(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }else
        super.onBackPressed();
    }

     private void animateNavigationDrawer() {

        //Add any color or remove it to use the default one!
        //To make it transparent use Color.Transparent in side setScrimColor();
        drawerLayout.setScrimColor(Color.TRANSPARENT);
        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

                // Scale the View based on current slide offset
                final float diffScaledOffset = slideOffset * (1 - END_SCALE);
                final float offsetScale = 1 - diffScaledOffset;
                contentView.setScaleX(offsetScale);
                contentView.setScaleY(offsetScale);

                // Translate the View, accounting for the scaled width
                final float xOffset = drawerView.getWidth() * slideOffset;
                final float xOffsetDiff = contentView.getWidth() * diffScaledOffset / 2;
                final float xTranslation = xOffset - xOffsetDiff;
                contentView.setTranslationX(xTranslation);
            }
        });

    }
     @Override
     public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
       int id=menuItem.getItemId();
        switch (id){
           case R.id.create_project:
               DialogFragment dialog = FullscreenDialog.newInstance();
               ((FullscreenDialog) dialog).setCallback(new FullscreenDialog.Callback() {
                   @Override
                   public void onActionClick(String name) {
                       Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();
                   }
               });
               dialog.show(getSupportFragmentManager(), "tag");
               break;
           case R.id.nav_home:
               drawerLayout.closeDrawer(GravityCompat.START);
               getSupportFragmentManager()
                       .beginTransaction()
                       .replace(R.id.fragment_container, new MainFragment())
                       .commit();
               break;
           case R.id.nav_profile:
              // String dd="Mazkur ilovadan chiqish tugmasini \nbosgandan keyin dasturdan to'liq\nchiqib ketiladi va kirish uchun\nEMAIL va PAROLIZNI qaytadan\nkiritish lozim bo'ladi. Agar\n dasturni vaqtinchalik yopmoqchi \nbo'lsayiz 2 marta ortga tugmasini\nbosishiz kifoya.";
             //  functions.showAlertDialog(this,"Ilodadan chiqish","Hello world","Qabul qilish","Bekor qilish");
               DialogFragment dialog3 = Profile.newInstance();
               ((Profile) dialog3).setCallback(new Profile.Callback() {
                   @Override
                   public void onActionClick(String name) {
                       Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();
                   }
               });
               dialog3.show(getSupportFragmentManager(), "tag");
               break;
           case R.id.my_posts:
               DialogFragment dialog9 =new Elon_fragment();
               dialog9.show(getSupportFragmentManager(), "jafar2");
               break;
           case R.id.xabarlar:
               DialogFragment dialog2 = Xabarlar.newInstance();
               ((Xabarlar) dialog2).setCallback(new Xabarlar.Callback() {
                   @Override
                   public void onActionClick(String name) {
                       Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();
                   }
               });
               dialog2.show(getSupportFragmentManager(), "tag");
               //drawerLayout.closeDrawer(GravityCompat.START);
               break;
           case R.id.admin_panel:
               DialogFragment dialog4 = AdminPanel.newInstance();
               ((AdminPanel) dialog4).setCallback(new AdminPanel.Callback() {
                   @Override
                   public void onActionClick(String name) {
                       Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();
                   }
               });
               dialog4.show(getSupportFragmentManager(), "tag");
               break;
           case R.id.register_login:
               startActivity(new Intent(getApplicationContext(),RetailerStartupScreen.class));
               break;
           case R.id.about_program:
               DialogFragment dialog5 = Haqida.newInstance();
               ((Haqida) dialog5).setCallback(new Haqida.Callback() {
                   @Override
                   public void onActionClick(String name) {
                       Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();
                   }
               });
               dialog5.show(getSupportFragmentManager(), "tag");
           break;
           case R.id.logout:
               String tokenString=String.valueOf(token.getDisplayToken());
               Log.d("Token"," "+tokenString+token.getLoginStatus());
               final BottomSheetDialog  bottomSheetDialog2=new BottomSheetDialog(this,R.style.BottomsheetDemo);
               View view1= LayoutInflater.from(getApplicationContext())
                       .inflate(
                               R.layout.layout_bottom_sheet,
                               (LinearLayout)findViewById(R.id.bottomSheetContainer)
                       );
               view1.findViewById(R.id.buttonshare).setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       Toast.makeText(getApplicationContext(), "Logout..", Toast.LENGTH_SHORT).show();
                       token.setLoginStatus(false);
                       bottomSheetDialog2.dismiss();
                       finish();
                   }
               });
               bottomSheetDialog2.setContentView(view1);
               bottomSheetDialog2.show();
               break;
           default:
               Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
               break;
            }
        return true;
    }

    public boolean checkPermissionForReadExtertalStorage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int result = getApplicationContext().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            return result == PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }
}
