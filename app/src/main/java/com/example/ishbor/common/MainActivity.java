package com.example.ishbor.common;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ishbor.Asosiy_oyna;
import com.example.ishbor.R;

public class MainActivity extends AppCompatActivity {
    private static int SPLASH_SCREEN=4000;
    Animation topAnim,bottomAnim;
    private ImageView image;
    TextView slogan,logo;
    SharedPreferences sharedPreferences;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        //Hooks
        image = findViewById(R.id.imageView);
        logo = findViewById(R.id.textView);
        slogan = findViewById(R.id.textView2);
        topAnim = AnimationUtils.loadAnimation(this, R.anim.top_animation);
        bottomAnim = AnimationUtils.loadAnimation(this, R.anim.bottom_animation);
//Set animation to elements
        image.setAnimation(topAnim);
        logo.setAnimation(bottomAnim);
        slogan.setAnimation(bottomAnim);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                  // Attach all the elements those you want to animate in design
                sharedPreferences=getSharedPreferences("onBoardingScreen",MODE_PRIVATE);
                boolean isFirsttime=sharedPreferences.getBoolean("firstTime",true);
                if (isFirsttime){
                    SharedPreferences.Editor editor=sharedPreferences.edit();
                    editor.putBoolean("firstTime",false);
                    editor.commit();
                    Intent intent=new Intent(MainActivity.this, OnBoardingActivity.class);
                    Pair[]pairs=new Pair[2];
                    pairs[0]=new Pair<View, String>(image,"logo_image");
                    pairs[1]=new Pair<View, String>(logo,"logo_text");
                    ActivityOptions options= ActivityOptions.makeSceneTransitionAnimation(MainActivity.this,pairs);
                    startActivity(intent,options.toBundle());
                    finish();

                }else {
                    Intent intent=new Intent(MainActivity.this, Asosiy_oyna.class);
                     startActivity(intent);
                    finish();

                }
                    }
        },SPLASH_SCREEN);
    }
}
