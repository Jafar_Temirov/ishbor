package com.example.ishbor.Repository;
import android.app.Application;
import android.os.AsyncTask;
import androidx.lifecycle.LiveData;
import com.example.ishbor.Databases.DaoMain;
import com.example.ishbor.Databases.MainDatabase;
import com.example.ishbor.model.Job_information;
import java.util.List;

public class RoomRepository {
    private DaoMain daoMain;
    LiveData<List<Job_information>> listLiveData;

    public RoomRepository(Application application){
        MainDatabase  database=MainDatabase.getMainInformation(application);
        daoMain =database.daoMain();
        listLiveData=daoMain.getAllPosts();
    }

    public LiveData<List<Job_information>> getListLiveData(){
        return listLiveData;
    }

    public void insertData(List<Job_information> job_informations){
        new insertAsyncTask(daoMain).execute(job_informations);
    }

    private static class insertAsyncTask extends AsyncTask<List<Job_information>, Void, Void> {
        private DaoMain mAsyncTaskDao;
        insertAsyncTask(DaoMain dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final List<Job_information>... params) {
            mAsyncTaskDao.insertData(params[0]);
            return null;
        }
    }
}
