package com.example.ishbor.Repository;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.ishbor.model.Job_information;
import com.example.ishbor.network.ApiClient;
import com.example.ishbor.network.ApiService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.ishbor.fragment.fragment_bottom.MainFragment.TAG;

public class WebServiceRepository {
    Application application;
    private ApiService apiService;

    public WebServiceRepository(Application application) {
        this.application = application;
    }
    List<Job_information> webServiceRepositoy =new ArrayList<>();

    public LiveData<List<Job_information>> providesWebService(){
        final MutableLiveData<List<Job_information>> data=new MutableLiveData<>();
            apiService= ApiClient.getClient().create(ApiService.class);
            apiService.getAllData(1).enqueue(new Callback<List<Job_information>>() {
                @Override
                public void onResponse(@NonNull Call<List<Job_information>> call,@NonNull Response<List<Job_information>> response) {
                    Log.d("Repository","Response::::"+response.body());
                    if (response.isSuccessful()){
                         webServiceRepositoy=response.body();
                         //RoomRepository roomRepository=new RoomRepository(application);
                         //roomRepository.insertData(webServiceRepositoy);
                         data.setValue(webServiceRepositoy);
                         Log.d(TAG,"succes"+response.code());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<List<Job_information>> call,@NonNull Throwable t) {
                    Log.e("Jafar ",""+t.getLocalizedMessage());
                    data.setValue(null);
                }
            });
            return data;
    }
}
