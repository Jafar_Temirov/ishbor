package com.example.ishbor.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.ishbor.Repository.RoomRepository;
import com.example.ishbor.Repository.WebServiceRepository;
import com.example.ishbor.model.Job_information;

import java.util.List;

public class MainViewModel extends AndroidViewModel {

    // private RoomRepository roomRepository;
   // private LiveData<List<Job_information>> myAllPosts;
    private WebServiceRepository webServiceRepository;
    private LiveData<List<Job_information>> retroObservable;

    public MainViewModel(@NonNull Application application) {
        super(application);
        //roomRepository=new RoomRepository(application);
        webServiceRepository=new WebServiceRepository(application);
        retroObservable=webServiceRepository.providesWebService();
        //myAllPosts=roomRepository.getListLiveData();
    }
    public LiveData<List<Job_information>> getRetroObservable(){
        return retroObservable;
    }
}
