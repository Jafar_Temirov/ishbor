package com.example.ishbor;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import net.cachapa.expandablelayout.ExpandableLayout;

public class Category extends AppCompatActivity implements View.OnClickListener{
    private ExpandableLayout expandableLayout0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        expandableLayout0 = findViewById(R.id.expandable_layout_0);
        expandableLayout0.setOnExpansionUpdateListener(new ExpandableLayout.OnExpansionUpdateListener() {
            @Override
            public void onExpansionUpdate(float expansionFraction, int state) {
                Log.d("ExpandableLayout0", "State: " + state);
            }
        });

       findViewById(R.id.expand_button).setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if (!expandableLayout0.isExpanded()) {
            expandableLayout0.expand();
        } else {
            expandableLayout0.collapse();
        }
    }
}
