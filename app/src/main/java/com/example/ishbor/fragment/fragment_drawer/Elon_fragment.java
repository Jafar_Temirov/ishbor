package com.example.ishbor.fragment.fragment_drawer;
import android.content.Context;
import android.content.DialogInterface;
import android.media.Image;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.ishbor.R;
import com.example.ishbor.adapter.ElonAdapter;
import com.example.ishbor.adapter.ViewPagerAdapter_Elon;
import com.example.ishbor.model.Job_information;
import com.example.ishbor.network.ApiClient;
import com.example.ishbor.network.ApiService;
import com.example.ishbor.utils.Token;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.ishbor.login_register.LoginActivity.TAG;

public class Elon_fragment extends DialogFragment {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<Job_information> jobInformations;
    private ElonAdapter elonAdapter;
    private ElonAdapter.ItemClickListener itemClickListener;
    private ProgressBar progressBar;
    private Token token;
    private ImageView imageview;
    public Elon_fragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullscreenDialogtheme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_elon_fragment, container, false);
        token=new Token(getActivity());
        recyclerView=view.findViewById(R.id.recyclerview_elon);
        progressBar=view.findViewById(R.id.progressbar_elon);
        imageview=view.findViewById(R.id.back_elonfragment);
        layoutManager = new LinearLayoutManager(requireActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        String tok=token.getDisplayToken();
        LoadElonlar(tok);
        hideKeyboardInAndroidFragment(view);
        imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return view;
    }

     private void LoadElonlar(String token){
         Log.d(TAG,"Token "+token);
         progressBar.setVisibility(View.VISIBLE);
         ApiService apiInterface = ApiClient.getClient().create(ApiService.class);
         Call<List<Job_information>> listCall=apiInterface.getElonlar(token);
         listCall.enqueue(new retrofit2.Callback<List<Job_information>>() {
             @Override
             public void onResponse(@NonNull Call<List<Job_information>> call,@NonNull Response<List<Job_information>> response) {
                 progressBar.setVisibility(View.INVISIBLE);
                 if (response.isSuccessful()&&response.body()!=null){
                     Log.d(TAG,"Success ");
                     jobInformations=response.body();
                     Log.d(TAG,"the Size of data "+Integer.toString(jobInformations.size()));
                     elonAdapter=new ElonAdapter(jobInformations,requireActivity(),itemClickListener);
                     elonAdapter.notifyDataSetChanged();
                     recyclerView.setAdapter(elonAdapter);
                 }else Log.d(TAG,"Error ");

             }

             @Override
             public void onFailure(@NonNull Call<List<Job_information>> call,@NonNull Throwable t) {
                 progressBar.setVisibility(View.INVISIBLE);
                 Log.d(TAG,t.getLocalizedMessage());
             }
         });
     }

     public interface Callback {
        void onActionClick(String name);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
     }

      public static void hideKeyboardInAndroidFragment(View view){
        final InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
