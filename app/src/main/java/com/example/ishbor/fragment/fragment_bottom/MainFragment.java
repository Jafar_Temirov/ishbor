package com.example.ishbor.fragment.fragment_bottom;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ishbor.R;
import com.example.ishbor.adapter.MainAdapter;
import com.example.ishbor.model.Job_information;
import com.example.ishbor.network.ApiClient;
import com.example.ishbor.network.ApiService;
import com.example.ishbor.viewModel.MainViewModel;
import com.facebook.shimmer.ShimmerFrameLayout;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainFragment extends Fragment {
    private ApiService apiService,apiService2;
    private RecyclerView recyclerView;
    private MainAdapter mAdapter;
    private List<Job_information> job_informations;
    private MainAdapter.ItemClickListener itemClickListener;
    MainViewModel mainViewModel;
    private ShimmerFrameLayout shimmerFrameLayout;
    private RecyclerView.LayoutManager layoutManager;
    public static final String TAG="Mainactivity";

    public MainFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainViewModel=new ViewModelProvider(this).get(MainViewModel.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_main, container, false);
        recyclerView=view.findViewById(R.id.recyclerview_main);
        shimmerFrameLayout=view.findViewById(R.id.shimmerlayout);
        shimmerFrameLayout.startShimmer();
        layoutManager = new LinearLayoutManager(requireActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        LoadJson();
        /*  recyclerView.addItemDecoration(new MyDividerItemDecoration(getActivity(), LinearLayoutManager.HORIZONTAL, 10));
        apiService= ApiClient.getClient().create(ApiService.class);
        mainViewModel.getRetroObservable().observe(getViewLifecycleOwner(), new Observer<List<Job_information>>() {
            @Override
            public void onChanged(List<Job_information> job_informations) {
                if (job_informations!=null)
                displayData(job_informations);
                else Log.d(TAG,"Data Null");
            }
        });

         */

        itemClickListener=new MainAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d(TAG,"clciked "+position+" id "+job_informations.get(position).getId());

                DialogFragment dialog2 = new Doimiy_ishDetail();
                Bundle bundle=new Bundle();

                Job_information job_information=job_informations.get(position);
                bundle.putString("job_tel_number",job_information.getTel_number());
                bundle.putString("job_title",job_information.getJob_title());
                bundle.putString("job_desc",job_information.getJob_description());
                bundle.putString("job_cost",job_information.getCosts());
                bundle.putString("longtitude",job_information.getLongtitude());
                bundle.putString("latitude",job_information.getLatitude());
                bundle.putInt("user_id",job_information.getUser_id());
                bundle.putString("job_started_date",job_information.getJob_started_date());
                bundle.putString("job_ended_date",job_information.getJob_ended_date());
                bundle.putBoolean("job_love",job_information.getLove());
                bundle.putInt("job_info_id",job_information.getId());
                dialog2.setArguments(bundle);
                dialog2.show(getActivity().getSupportFragmentManager(), "tag");


            }
        };

        return view;
    }
    private void displayData(List<Job_information> posts) {
         mAdapter=new MainAdapter(getActivity(),posts,itemClickListener);
         mAdapter.notifyDataSetChanged();
         recyclerView.setAdapter(mAdapter);
         shimmerFrameLayout.stopShimmer();
    }

    private void LoadJson(){
        shimmerFrameLayout.startShimmer();
        ApiService apiService= ApiClient.getClient().create(ApiService.class);
        Call<List<Job_information>> listCall=apiService.getAllData(1);
        listCall.enqueue(new Callback<List<Job_information>>() {
            @Override
            public void onResponse(Call<List<Job_information>> call, Response<List<Job_information>> response) {
                shimmerFrameLayout.stopShimmer();
                if (response.isSuccessful() && response.body()!= null) {
                    job_informations=response.body();
                    mAdapter=new MainAdapter(requireActivity(),job_informations,itemClickListener);
                    mAdapter.notifyDataSetChanged();
                    recyclerView.setAdapter(mAdapter);

                }
                else Log.d(TAG,"onresponse made error");

            }

            @Override
            public void onFailure(Call<List<Job_information>> call, Throwable t) {
                shimmerFrameLayout.stopShimmer();
//                Toast.makeText(getActivity(), ""+t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    @Override
    public void onResume() {
        super.onResume();
        shimmerFrameLayout.stopShimmer();
    }
    @Override
    public void onPause() {
        shimmerFrameLayout.stopShimmer();
        super.onPause();
    }
}
