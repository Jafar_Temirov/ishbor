package com.example.ishbor.fragment.fragment_bottom;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ishbor.R;
import com.example.ishbor.adapter.TelegramAdapter;
import com.example.ishbor.model.Chat;
import com.example.ishbor.network.ApiClient;
import com.example.ishbor.network.ApiService;
import com.example.ishbor.utils.Functions;
import com.example.ishbor.utils.Token;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.example.ishbor.fragment.fragment_bottom.MainFragment.TAG;

public class ProfileFragmen extends DialogFragment {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private TelegramAdapter telegramAdapter;
    private List<Chat> chats;
    private ProgressBar progressBar;
    private ImageView back,send;
    private EditText editText;
    private TextView title,yozilmagan;
    private  int user_id,job_info_id;
    private RelativeLayout relativeLayout;
    private Functions functions;
    private Token token;
    private String my_id;
    private int id;
    public ProfileFragmen() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullscreenDialogtheme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_profile, container, false);
        token=new Token(getActivity());
        progressBar=view.findViewById(R.id.progressbar_telegram);
        relativeLayout=view.findViewById(R.id.relativelayout_chat);
        yozilmagan=view.findViewById(R.id.chat_yozilmagan);
      //  title=view.findViewById(R.id.chat_title);
        back=view.findViewById(R.id.chat_back);
        send=view.findViewById(R.id.chat_telegram);
        editText=view.findViewById(R.id.chat_edit);
        recyclerView=view.findViewById(R.id.recyclerview_telegram);
        layoutManager = new LinearLayoutManager(requireActivity(),LinearLayoutManager.VERTICAL,true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        Bundle margs=getArguments();
        user_id=margs.getInt("user_id",0);
        job_info_id=margs.getInt("job_info_id",0);
        Log.d(TAG,"Job info id  "+job_info_id+"user id "+user_id);
        my_id=token.getDisplayToken();
        id=token.getDisplayId();
        LoadData(job_info_id,my_id,user_id);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edit=editText.getText().toString().trim();
                if (edit!=null&&!edit.isEmpty()){
                    Log.d(TAG,"yes");
                    saveData(job_info_id,my_id,id,user_id,edit);
                }else{
                    Log.d(TAG,"No");
                    // functions.snackbar(relativeLayout,"Malumotni kiriting.");
                    // Xatolik shu Toastda chiqyapti
                   // Toast.makeText(requireContext(), "Malumotni kiriting", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    private void saveData(int job_info, String token,int id, int partner_id, String text){
        ApiService apiInterface2 = ApiClient.getClient().create(ApiService.class);
        Call<Chat> call=apiInterface2.saveChat(job_info,token,id,partner_id,text);
        call.enqueue(new retrofit2.Callback<Chat>() {
            @Override
            public void onResponse(@NonNull Call<Chat> call,@NonNull  Response<Chat> response) {
             if (response.isSuccessful()&&response.body()!=null){
                 Log.d(TAG,"success");
                 editText.setText("");
                 LoadData(job_info_id,my_id,user_id);
                 // telegramAdapter.notifyDataSetChanged();
                // recyclerView.setAdapter(telegramAdapter);
             }
             else  Log.d(TAG,"error");
            }
            @Override
            public void onFailure(@NonNull  Call<Chat> call,@NonNull  Throwable t) {
                Log.d(TAG,t.getLocalizedMessage()+"");
            }
        });

    }

    private void LoadData(int job_info_id,String my_token, int partner_id) {
        progressBar.setVisibility(View.VISIBLE);
        ApiService apiInterface = ApiClient.getClient().create(ApiService.class);
        Call<List<Chat>> listCall=apiInterface.getChat(job_info_id,my_token,partner_id);
        listCall.enqueue(new retrofit2.Callback<List<Chat>>() {
            @Override
            public void onResponse(Call<List<Chat>> call, Response<List<Chat>> response) {
                progressBar.setVisibility(View.INVISIBLE);
                if (response.isSuccessful()&&response.body()!=null){
                    int size=response.body().size();
                    if (size>0){
                        yozilmagan.setVisibility(View.INVISIBLE);
                        Log.d(TAG,"success "+size);
                        chats=response.body();
                        telegramAdapter=new TelegramAdapter(requireActivity(),chats);
                        telegramAdapter.notifyDataSetChanged();
                        recyclerView.setAdapter(telegramAdapter);
                    }else {
                        yozilmagan.setVisibility(View.VISIBLE);
                    }
                }else
                Log.d(TAG,"Error");
            }

            @Override
            public void onFailure(@NonNull Call<List<Chat>> call,@NonNull Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
               // Toast.makeText(getActivity(), ""+t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public interface Callback {void onActionClick(String name);}

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }
}
