package com.example.ishbor.fragment.fragment_drawer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.ishbor.R;
import com.example.ishbor.adapter.ElonAdapter;
import com.example.ishbor.model.Job_information;
import com.example.ishbor.network.ApiClient;
import com.example.ishbor.network.ApiService;
import com.example.ishbor.utils.Token;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.example.ishbor.login_register.LoginActivity.TAG;

public class Xabarlar extends DialogFragment {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<Job_information> jobInformations;
    private ElonAdapter elonAdapter;
    private ElonAdapter.ItemClickListener itemClickListener;
    private ProgressBar progressBar;
    private Callback callback;
    private ImageView imageView;
    private Token token;

    public static Xabarlar newInstance() {
        return new Xabarlar();
    }
    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public Xabarlar() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullscreenDialogtheme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_xabarlar, container, false);
        imageView=view.findViewById(R.id.back_iconxabarlarfragment);
        recyclerView=view.findViewById(R.id.recyclerview_xabar);
        progressBar=view.findViewById(R.id.progressbar_xabarlar);
        layoutManager = new LinearLayoutManager(requireActivity());
        token=new Token(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        String tok=token.getDisplayToken();
        //LoadElonlar(tok);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return  view;
    }
    private void LoadElonlar(String token){
        Log.d(TAG,"Token "+token);
        progressBar.setVisibility(View.VISIBLE);
        ApiService apiInterface = ApiClient.getClient().create(ApiService.class);
        Call<List<Job_information>> listCall=apiInterface.getElonlar(token);
        listCall.enqueue(new retrofit2.Callback<List<Job_information>>() {
            @Override
            public void onResponse(@NonNull Call<List<Job_information>> call, @NonNull Response<List<Job_information>> response) {
                progressBar.setVisibility(View.INVISIBLE);
                if (response.isSuccessful()&&response.body()!=null){
                    Log.d(TAG,"Success ");
                    jobInformations=response.body();
                    Log.d(TAG,"the Size of data "+Integer.toString(jobInformations.size()));
                    elonAdapter=new ElonAdapter(jobInformations,requireActivity(),itemClickListener);
                    elonAdapter.notifyDataSetChanged();
                    recyclerView.setAdapter(elonAdapter);
                }else Log.d(TAG,"Error ");

            }

            @Override
            public void onFailure(@NonNull Call<List<Job_information>> call,@NonNull Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                Log.d(TAG,t.getLocalizedMessage());
            }
        });
    }


    public interface Callback {
        void onActionClick(String name);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }
}
