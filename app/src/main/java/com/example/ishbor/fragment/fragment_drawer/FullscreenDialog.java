package com.example.ishbor.fragment.fragment_drawer;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import com.example.ishbor.R;
import com.example.ishbor.model.Category;
import com.example.ishbor.model.Job_information;
import com.example.ishbor.network.ApiClient;
import com.example.ishbor.network.ApiService;
import com.example.ishbor.utils.Token;
import com.google.android.material.textfield.TextInputEditText;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import retrofit2.Call;
import retrofit2.Response;
import static android.app.Activity.RESULT_OK;

public class FullscreenDialog  extends DialogFragment  implements View.OnClickListener{

    private static final String TAG="MainActivity";
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;
    private Callback callback;
    private CheckBox checkBox;
    private Button btn_started,btn_ended,btn_map;
    private EditText end_calendar,start_calendar,cost;
    private TextInputEditText title,tarif_ish,telefon_number;
    private Calendar calendar=Calendar.getInstance();
    public static ApiService apiService2;
    // Spinner task
    private Spinner spinner;
    private List<Category> kategoriya;
    private List<String> codes;
    private List<Integer> idss;
    private int mGender = 1;
    private String longtitude,latitude;
    private RadioGroup radiogroup;
    private RadioButton temp,perm;
    private Button fullscreen_data;
    private Token token;
    public static FullscreenDialog newInstance() {
        return new FullscreenDialog();
    }
    public void setCallback(Callback callback) {
        this.callback = callback;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setStyle(DialogFragment.STYLE_NORMAL, R.style.FullscreenDialogtheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fullscreen_dialog, container, false);
        ImageButton close = view.findViewById(R.id.cancelBtn);
        ImageButton action = view.findViewById(R.id.editBtn8);
        token=new Token(getActivity());
        fullscreen_data=view.findViewById(R.id.fullscreen_elon);
        telefon_number=view.findViewById(R.id.fullscreen_tel_number);
        radiogroup=view.findViewById(R.id.radiogroup);
        btn_map=view.findViewById(R.id.location_btn);
        start_calendar=view.findViewById(R.id.start_calendar);
        end_calendar=view.findViewById(R.id.ended_calendar);
        tarif_ish=view.findViewById(R.id.fullscreen_tariflar);
        title=view.findViewById(R.id.fullscreen_sarlavha);
        spinner=view.findViewById(R.id.spinner_create_pj);
        checkBox=view.findViewById(R.id.kelishilgan_checkbox);
        cost=view.findViewById(R.id.ish_haqi_cp);

        codes = new ArrayList<String>();
        idss = new ArrayList<Integer>();
        start_calendar.setFocusableInTouchMode(false);
        start_calendar.setFocusable(false);
        end_calendar.setFocusableInTouchMode(false);
        end_calendar.setFocusable(false);
        close.setOnClickListener(this);
        action.setOnClickListener(this);
        fullscreen_data.setOnClickListener(this);
        btn_map.setOnClickListener(this);
        start_calendar.setOnClickListener(this);
        end_calendar.setOnClickListener(this);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               if (checkBox.isChecked()){
                   cost.setFocusableInTouchMode(false);
                   cost.setFocusable(false);
               }
               else {
                   cost.setFocusableInTouchMode(true);
                   cost.setFocusable(true);
               }
            }
       });

        LoadData();
     return view;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.cancelBtn:
                dismiss();
                break;
            case R.id.editBtn8:
                callback.onActionClick("Whatever");
                dismiss();
                break;
            case R.id.fullscreen_elon:
                String job_title, job_description,job_owners,costs,tel_number,job_started_date,job_ended_date;
                int category_id=mGender;
                int user_id=token.getDisplayId();
                job_title=title.getText().toString().trim();
                job_description=tarif_ish.getText().toString().trim();
                costs=cost.getText().toString().trim();
                tel_number=telefon_number.getText().toString().trim();
                job_started_date=start_calendar.getText().toString().trim();
                job_ended_date=end_calendar.getText().toString().trim();
                int chackedid=radiogroup.getCheckedRadioButtonId();
                int job_type=findRadio(chackedid);
                Log.d(TAG,"Clicked");
                    if (job_title!=null&&!job_title.isEmpty()&&
                        job_description!=null&&!job_description.isEmpty()&&
                        costs!=null&&!costs.isEmpty()&&
                        tel_number!=null&&!tel_number.isEmpty()&&
                        job_started_date!=null&&!job_started_date.isEmpty()&&
                        job_ended_date!=null&&!job_ended_date.isEmpty()&&
                        job_type!=0&&user_id!=0){
                        Log.d(TAG,"To'liq kiritildi");
                    apiService2=ApiClient.getClient().create(ApiService.class);
                    Call<Job_information> data=apiService2.Save_Job(job_title,job_description,costs,tel_number,user_id,longtitude,latitude,job_started_date,job_ended_date,category_id,job_type);
                    data.enqueue(new retrofit2.Callback<Job_information>() {
                        @Override
                        public void onResponse(@NonNull Call<Job_information> call,@NonNull Response<Job_information> response) {
                            if (response.isSuccessful()&&response.body()!=null) {
                                if (response.body().getSuccess()&&response.body().getMessage().equals("inserted")){
                                    Log.d(TAG,"Saqlandi !!!");
                                    dismiss();
                                }
                                else  Log.d(TAG,"Saqlanmadi !!!");
                            }else Log.d(TAG,"Xatolik serverda");
                        }
                        @Override
                        public void onFailure(@NonNull Call<Job_information> call, @NonNull Throwable t) {
                            Log.d(TAG,""+t.getLocalizedMessage());
                        }
                    });
                }else Log.d(TAG,"Malumotlarni to'liq kiriting !!!");
                break;
            case R.id.location_btn:
                Intent intent=new Intent(getActivity(),MapActivity.class);
                startActivityForResult(intent,SECOND_ACTIVITY_REQUEST_CODE);
                break;
            case R.id.start_calendar:
                new DatePickerDialog(requireActivity(), date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.ended_calendar:
                new DatePickerDialog(requireActivity(), date2, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
        }
    }

    private int findRadio(int chackedid) {
    int i=1;
    if (chackedid==R.id.radio_permenant){
        Log.d(TAG,"Permenant");
        i=1;

    }else if (chackedid==R.id.radio_temporary){
        Log.d(TAG,"Temporary");
        //Toast.makeText(getActivity(), "Temporary", Toast.LENGTH_SHORT).show();
        i=2;
    }
     return i;
    }

    public interface Callback {
        void onActionClick(String name);
    }
    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth){
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setBirth();
        }

    };
    DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth){
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setBirth2();
        }

    };
    private void setBirth2() {
        String myFormat = "dd MMMM yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        end_calendar.setText(sdf.format(calendar.getTime()));
    }
    private void setBirth() {
        String myFormat = "dd MMMM yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        start_calendar.setText(sdf.format(calendar.getTime()));
    }
    private void LoadData() {
        ApiService serviceApi= ApiClient.getClient().create(ApiService.class);
        Call<List<Category>> call=serviceApi.getCategory();
        call.enqueue(new retrofit2.Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    kategoriya=response.body();
                    for (int i=0;i<kategoriya.size();i++){
                        codes.add(kategoriya.get(i).getCategory_name());
                        idss.add(kategoriya.get(i).getId());
                        Log.d(TAG,"id "+idss+" "+codes);
                    }
                    setupSpinner();
                }else {
                   // MainActivity.appPreference.showToast("Xatolik sodir bo'ldi");
                    Log.e(TAG,"error");
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
             Log.e(TAG,t.getLocalizedMessage());
            }
        });
    }
    private void setupSpinner(){
        ArrayAdapter<String> genderSpinnerAdapter =new  ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,codes);
        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(genderSpinnerAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    // Toast.makeText(Moderator_Add.this,selection, Toast.LENGTH_SHORT).show();
                    mGender=idss.get(position);
                    Log.d(TAG,"Gender "+mGender);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                mGender=1;
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==SECOND_ACTIVITY_REQUEST_CODE&&resultCode==RESULT_OK){
            String returnString = data.getStringExtra("adress");
            String[] array=returnString.split(",");
            String ll= array[0]+array[1];
            longtitude=array[0];latitude=array[1];
            Log.d(TAG,ll);
        }
    }
}
