package com.example.ishbor.fragment.fragment_drawer;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.example.ishbor.R;
import com.example.ishbor.adapter.ViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;

public class AdminPanel extends DialogFragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private Callback callback;

    public static AdminPanel newInstance() {
        return new AdminPanel();}
    public void setCallback(Callback callback) {
        this.callback = callback;}

    public AdminPanel() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullscreenDialogtheme);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_admin_panel, container, false);
        tabLayout=view.findViewById(R.id.tablayout_id);
        viewPager=view.findViewById(R.id.viewpager_id);
        adapter=new ViewPagerAdapter(getChildFragmentManager());

        adapter.AddFragment(new CreatePreject(),"Tasdiqlangan \n Ma'lumotlar");
        adapter.AddFragment(new CreatePreject(),"Tasdiqlanmagan  \n Ma'lumotlar");
        adapter.AddFragment(new CreatePreject(),"Foydalanuvchilar");
        adapter.AddFragment(new CreatePreject(),"Kategoriyalar");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        return view;
    }

    public interface Callback {
        void onActionClick(String name);}

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

}
