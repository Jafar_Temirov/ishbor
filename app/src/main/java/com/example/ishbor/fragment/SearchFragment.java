package com.example.ishbor.fragment;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.ishbor.R;
import com.example.ishbor.adapter.search.SearchAdapter;
import com.example.ishbor.adapter.search.SearchPresenter;
import com.example.ishbor.fragment.fragment_bottom.Doimiy_ishDetail;
import com.example.ishbor.model.Job_information;

import java.util.List;

import static com.example.ishbor.fragment.Uy_fragment.TAG;

public class SearchFragment extends DialogFragment implements com.example.ishbor.adapter.search.SearchView {
    private static final int INTENT_EDIT = 200;
    private static final int INTENT_ADD = 100;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SearchAdapter callAdapter;
    private SearchAdapter.ItemClickListener itemClickListener;
    private List<Job_information> note;
    private SearchView searchView;
    private ImageView imageView;
    SearchPresenter  searchPresenter;

    public SearchFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullscreenDialogtheme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_search, container, false);
        searchView=view.findViewById(R.id.searchview_call);
        search();
        swipeRefreshLayout=view.findViewById(R.id.swipe_refresh_search);
        recyclerView =view.findViewById(R.id.recycler_search);
        imageView=view.findViewById(R.id.back_icon_search);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        searchPresenter=new SearchPresenter(this);
        searchPresenter.getData();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() { searchPresenter.getData();
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        itemClickListener=new SearchAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d(TAG,"clciked"+position);
                DialogFragment dialog2 = new Doimiy_ishDetail();
                Bundle bundle=new Bundle();
                Job_information job_information=note.get(position);
                bundle.putString("job_tel_number",job_information.getTel_number());
                bundle.putString("job_title",job_information.getJob_title());
                bundle.putString("job_desc",job_information.getJob_description());
                bundle.putString("job_cost",job_information.getCosts());
                bundle.putString("longtitude",job_information.getLongtitude());
                bundle.putString("latitude",job_information.getLatitude());
                bundle.putInt("user_id",job_information.getUser_id());
                bundle.putString("job_started_date",job_information.getJob_started_date());
                bundle.putString("job_ended_date",job_information.getJob_ended_date());
                bundle.putBoolean("job_love",job_information.getLove());
                bundle.putInt("job_info_id",job_information.getId());
                dialog2.setArguments(bundle);
                dialog2.show(getActivity().getSupportFragmentManager(), "tag");
            }
        };

    return view;
    }
    private void search(){
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setQueryHint("Malumotlarni qidiring ..!");
        searchView.setIconifiedByDefault(false);
        searchView.onActionViewExpanded();
        searchView.clearFocus();
        searchView.setFocusable(false);
        searchView.setFocusableInTouchMode(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (callAdapter!=null) {
                    Log.d(TAG,"Query "+query);
                    callAdapter.getFilter().filter(query);
                }return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (callAdapter!=null)
                    Log.d(TAG,"Query "+newText);
                    callAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public void showLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onGetResult(List<Job_information> notes) {
        callAdapter=new SearchAdapter(getActivity(),notes,itemClickListener);
        callAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(callAdapter);
        note=notes;
    }
}