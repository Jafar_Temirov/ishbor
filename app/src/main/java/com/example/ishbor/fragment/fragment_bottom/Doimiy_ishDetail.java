package com.example.ishbor.fragment.fragment_bottom;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.ishbor.R;
import com.example.ishbor.model.Job_information;
import com.example.ishbor.network.ApiClient;
import com.example.ishbor.network.ApiService;
import com.example.ishbor.utils.Token;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

import static com.example.ishbor.fragment.fragment_bottom.MainFragment.TAG;

public class Doimiy_ishDetail extends DialogFragment {
    private Callback callback;
    private Button job_description,job_cost;
    private TextView job_started_date,job_ended_date,job_title,job_location;
    private String longtitude,latitude,started_date,ended_date, title, desc, cost, tel_number;
    private Boolean love;
    private ImageView job_love,back;
    private Token token;
    private LinearLayout linearLayout;
    int user_id,job_info_id;
    private Button sms,call;

    public static Doimiy_ishDetail newInstance(){
            return new Doimiy_ishDetail();
    }
    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullscreenDialogtheme);}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.doimiy_ishdetail, container, false);
        sms=view.findViewById(R.id.ishdetail_sms);
        call=view.findViewById(R.id.ishdetail_call);
        back=view.findViewById(R.id.ishdetail_back);
        job_title=view.findViewById(R.id.ishdetail_title);
        job_description=view.findViewById(R.id.ishdetail_description);
        job_cost=view.findViewById(R.id.ishdetail_narxi);
        job_love=view.findViewById(R.id.ishdetail_love);
        job_started_date=view.findViewById(R.id.ishdetail_started_date);
        job_ended_date=view.findViewById(R.id.ishdetail_ended_date);
        job_location=view.findViewById(R.id.ishdetail_manzil);
        linearLayout=view.findViewById(R.id.linear_doimiyish1);
        token=new Token(getActivity());

        Bundle margs=getArguments();
        tel_number=margs.getString("job_tel_number");
        desc=margs.getString("job_desc");
        cost=margs.getString("job_cost");
        title=margs.getString("job_title");
        longtitude=margs.getString("longtitude");
        latitude=margs.getString("latitude");
        ended_date=margs.getString("job_ended_date");
        started_date=margs.getString("job_started_date");
        love=margs.getBoolean("job_love",false);
        user_id=margs.getInt("user_id",0);
        job_info_id=margs.getInt("job_info_id",0);
        if (love){
            job_love.setImageResource(R.drawable.likeon);
        } else {
            job_love.setImageResource(R.drawable.likeof);
        }
        job_title.setText(title);
        job_cost.setText(cost);
        job_description.setText(desc);
        job_cost.setText("Narxi:  "+cost);

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callintent = new Intent(Intent.ACTION_DIAL);
                if (tel_number!=null&&!tel_number.isEmpty()){
                    callintent.setData(Uri.parse("tel:"+tel_number));
                    getActivity().startActivity(callintent);
                }
            }
        });

        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (token.getLoginStatus()){

                    DialogFragment dialog12 = new ProfileFragmen();
                    Bundle bundle=new Bundle();
                    Log.d("Jafar","Job: "+job_info_id+"+"+user_id);
                    bundle.putInt("job_info_id",job_info_id);
                    bundle.putInt("user_id",user_id);
                    dialog12.setArguments(bundle);
                    dialog12.show(getChildFragmentManager(), "tag");
                  //  Toast.makeText(getActivity(), "Registratsiyadan o'tildi!!!", Toast.LENGTH_SHORT).show();
                }else {
                    Log.d(TAG,"Registratsiyadan o'ting!!");
                  //  Toast.makeText(getActivity(), "Registratsiyadan o'ting!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String geoUri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr="+ latitude + "," + longtitude + " (" + title+ ")");
                Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                getActivity().startActivity(intent1);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        job_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (love){
                      job_love.setImageResource(R.drawable.likeof);
                      updateLove(false);
                }else {
                    job_love.setImageResource(R.drawable.likeon);
                    updateLove(true);
                }
            }


        });
        return view;
    }


    private void updateLove(Boolean love) {
        String tok=token.getDisplayToken();
        ApiService apiInterface2 = ApiClient.getClient().create(ApiService.class);
        Call<Job_information>  call=apiInterface2.updateLove(tok,job_info_id,love);
        call.enqueue(new retrofit2.Callback<Job_information>() {
            @Override
            public void onResponse(Call<Job_information> call, Response<Job_information> response) {
                if (response.isSuccessful()&&response.body()!=null){
                    if (response.body().getSuccess()){
                        Log.d(TAG,response.body().getMessage());
                    }else   Log.d(TAG,response.body().getMessage());
                    Log.d(TAG,"success");
                }else   Log.d(TAG,"error");
            }
            @Override
            public void onFailure(Call<Job_information> call, Throwable t) {
                Log.d(TAG,t.getLocalizedMessage());
            }
        });
    }

    public interface Callback {
        void onActionClick(String name);
    }
    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

}
