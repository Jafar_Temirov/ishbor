package com.example.ishbor.fragment.fragment_drawer;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.ishbor.R;
import com.example.ishbor.model.User;
import com.example.ishbor.network.ApiClient;
import com.example.ishbor.network.ApiService;
import com.example.ishbor.utils.Token;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.ishbor.login_register.LoginActivity.TAG;

public class Profile extends DialogFragment {
    private Callback callback;
    private EditText username,password, email,tel_number;
    private Button update;
    private User user;
    private Token token;
    private ImageView read_image,read_permission,back;
    private ProgressBar progressBar;
    public static Profile newInstance() {
        return new Profile();
    }
    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public Profile() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullscreenDialogtheme);}

    public interface Callback {
        void onActionClick(String name);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_profile2, container, false);
        read_image=view.findViewById(R.id.image_profile);
        back=view.findViewById(R.id.meening_profileback);
        username=view.findViewById(R.id.profile_username);
        password=view.findViewById(R.id.profile_password);
        email=view.findViewById(R.id.profile_surname);
        tel_number=view.findViewById(R.id.profile_phonenumber);
        update=view.findViewById(R.id.update_profile);
        read_permission=view.findViewById(R.id.profile_permission);
        progressBar=view.findViewById(R.id.progressBar_profil);
        readMode();
        hideKeyboardInAndroidFragment(view);
        token=new Token(getActivity());
        String myid=token.getDisplayToken();
        if (token.getLoginStatus()){

            ApiService apiInterface2 = ApiClient.getClient().create(ApiService.class);
            Call<User> userCall=apiInterface2.IdentifyUser(myid);
            userCall.enqueue(new retrofit2.Callback<User>() {
                @Override
                public void onResponse(@NonNull Call<User> call,@NonNull Response<User> response) {
                    if (response.isSuccessful()&&response.body()!=null){
                        if (response.body().getSuccess()&&response.body().getMessage().equals("exist")){
                          username.setText(response.body().getUsername().trim());
                          tel_number.setText(response.body().getPhone_number().trim());
                          email.setText(response.body().getSurname().trim());
                          password.setText("*******");

                          Log.d(TAG,"Password:::"+password);
                          Log.d(TAG,response.body().getUsername()+response.body().getPhone_number());

                        }else Log.d(TAG,"mavjud emas");
                    }
                    else Log.d(TAG,"Xatolik");
                }

                @Override
                public void onFailure(@NonNull Call<User> call,@NonNull Throwable t) {
                    Log.d(TAG,"serverda Xatolik");
                }
            });

        }else {
            Log.d(TAG,"Login yoki Registratriyadan o'ting");
        }

        read_permission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Log.d(TAG,"onclick");
              editMode();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user,surname,pass,number;
                user=username.getText().toString().trim();
                surname=email.getText().toString().trim();
                pass=password.getText().toString().trim();
                number=tel_number.getText().toString().trim();
                if (user!=null&&!user.isEmpty()&&surname!=null&&!surname.isEmpty()&&pass!=null&&!pass.isEmpty()&&number!=null&&!number.isEmpty()){
                    updateUser(
                            user,
                            surname,
                            pass,
                            number,
                            token.getDisplayToken());
                }else {
                    Log.d(TAG,"Ma'lumotni to'liq kiriting !!!");
                }

            }
        });
        return view;
    }

    public void updateUser(String  username,String surname,String password,String tel_number,String tokenlar){
        progressBar.setVisibility(View.VISIBLE);
        ApiService apiInterface3 = ApiClient.getClient().create(ApiService.class);
        Call<User> userCall=apiInterface3.updateUser(username,surname,password,tel_number,tokenlar);
        userCall.enqueue(new retrofit2.Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call,@NonNull Response<User> response) {
                progressBar.setVisibility(View.INVISIBLE);
                if (response.isSuccessful()&&response.body()!=null){
                    Log.d(TAG,"Success");
                    if (response.body().getSuccess()&&response.body().getMessage().equals("inserted")){
                        Log.d(TAG,"Saqlandi "+response.body().getToken()+response.body().getId());
                         //dismiss();
                        update.setEnabled(false);
                    }else  Log.d(TAG,"Saqlanmadi");
                }else  Log.d(TAG,"Serverda Xatolik");
            }

            @Override
            public void onFailure(@NonNull Call<User> call,@NonNull Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                Log.d(TAG,"Error");
            }
        });
    }

    public void readMode(){
        update.setEnabled(false);
        update.setClickable(false);
        username.setFocusableInTouchMode(false);
        password.setFocusableInTouchMode(false);
        email.setFocusableInTouchMode(false);
        tel_number.setFocusableInTouchMode(false);
    }
    public void editMode(){
        update.setEnabled(true);
        update.setClickable(true);
        username.setFocusableInTouchMode(true);
        password.setFocusableInTouchMode(true);
        password.setText("");
        email.setFocusableInTouchMode(true);
        tel_number.setFocusableInTouchMode(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    public static void hideKeyboardInAndroidFragment(View view){
        final InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), getTheme()) {
            @Override public void cancel() {
                if (getActivity() != null && getView() != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                }
                super.cancel();
            }
        };
        return dialog;
    }
}