package com.example.ishbor.fragment;
import android.os.Bundle;

import androidx.core.view.ViewCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.ishbor.R;
import com.example.ishbor.adapter.UyMainAdapter;
import com.example.ishbor.fragment.fragment_drawer.Xabarlar;
import com.example.ishbor.model.UyModel;
import com.example.ishbor.network.ApiClient;
import com.example.ishbor.network.ApiService;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Uy_fragment extends Fragment implements  SwipeRefreshLayout.OnRefreshListener{
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<UyModel> uyModels = new ArrayList<>();
    private UyMainAdapter uyMainAdapter;
    public static final String TAG="Mainactivity";
    private SwipeRefreshLayout swipeRefreshLayout;
    private UyMainAdapter.OnItemClickListener onItemClickListener;

    public Uy_fragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_uy_fragment, container, false);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout_uy);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);

        recyclerView = view.findViewById(R.id.recyclerView_uy);
        layoutManager = new LinearLayoutManager(requireActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        onLoadingSwipeRefresh();
        LoadJson();
        onItemClickListener=new UyMainAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d(TAG,"click "+position);
                DialogFragment uyDetailFragment=new UyDetailFragment();
                Bundle bundle=new Bundle();
                ImageView imageView = view.findViewById(R.id.img);
                UyModel uymodel=uyModels.get(position);
                bundle.putString("home_owner",uymodel.getHome_owner());
                bundle.putString("tel_number",uymodel.getTel_number());
                bundle.putString("uy_haqida",uymodel.getUy_haqida());
                bundle.putString("uy_manzili",uymodel.getUy_manzili());
                bundle.putString("narxi",uymodel.getNarxi());
                bundle.putString("posted_at",uymodel.getPosted_at());
                bundle.putString("image_row",uymodel.getImage_row());
                uyDetailFragment.setArguments(bundle);
                uyDetailFragment.show(getActivity().getSupportFragmentManager(),"tag");
              /*  getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .addSharedElement(imageView, ViewCompat.getTransitionName(imageView))
                        .replace(R.id.fragment_container,uyDetailFragment,"activity")
                        .addToBackStack(null)
                        .commit();
               */
            }
        };
        return view;
    }
    public void LoadJson(){
        swipeRefreshLayout.setRefreshing(true);
        ApiService apiInterface = ApiClient.getClient().create(ApiService.class);
        Call<List<UyModel>> call= apiInterface.getAllUyMain();
        call.enqueue(new Callback<List<UyModel>>() {
            @Override
            public void onResponse(Call<List<UyModel>> call, Response<List<UyModel>> response) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful() && response.body()!= null) {
                  uyModels=response.body();
                  uyMainAdapter=new UyMainAdapter(requireActivity(),uyModels,onItemClickListener);
                  uyMainAdapter.notifyDataSetChanged();
                  recyclerView.setAdapter(uyMainAdapter);
                  Log.d(TAG,"onresponse made success");
                }else Log.d(TAG,"onresponse made error");

            }
            @Override
            public void onFailure(Call<List<UyModel>> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(requireActivity(), ""+t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRefresh() {
        LoadJson();
    }
    private void onLoadingSwipeRefresh(){
        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        LoadJson();
                    }
                }
        );

    }
}
