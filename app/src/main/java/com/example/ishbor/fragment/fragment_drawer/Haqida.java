package com.example.ishbor.fragment.fragment_drawer;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.example.ishbor.R;

public class Haqida extends DialogFragment implements View.OnClickListener{
    private Callback callback;
    private ImageView back,telegram;
    private Button button;


    public static Haqida newInstance() {
        return new Haqida();
    }
    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public Haqida(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullscreenDialogtheme);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_haqida, container, false);
        telegram=view.findViewById(R.id.telegram);
        back=view.findViewById(R.id.back_icon);
        button=view.findViewById(R.id.btn_number);
        telegram.setOnClickListener(this);
        back.setOnClickListener(this);
        button.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_icon:
                dismiss();
                break;
            case R.id.telegram:
                Intent telegram = new Intent(Intent.ACTION_VIEW , Uri.parse("https://telegram.me/jafardev"));
                startActivity(telegram);
                break;
            case R.id.btn_number:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+998906108036"));
                startActivity(intent);
                break;

        }
    }


    public interface Callback {
        void onActionClick(String name);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

}
